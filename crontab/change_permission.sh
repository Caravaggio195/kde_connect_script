#!/bin/bash
#insert in crontab -e
# @reboot dir/kde_connect_script/crontab/change_permission.sh
#Author: Domenico Caravaggio
FILE_PATH="/sys/class/backlight/intel_backlight/brightness"
sudo chown root.staff "$FILE_PATH"
sudo chmod 664 "$FILE_PATH"
exit 0
