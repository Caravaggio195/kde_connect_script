#!/bin/bash
#script  brightness
#Author: Domenico Caravaggio
thisdir="/home/domenico/bin"
path_brightness="/sys/class/backlight/intel_backlight"
brightness=$( cat "$path_brightness/brightness" )
max_brightness=$( cat "$path_brightness/max_brightness" )
min_brightness=0
passo=0
let passo=(max_brightness-min_brightness)/10
echo " 1:$1 brightness: $brightness max: $max_brightness min : $min_brightness "

if [[ "$1" == "-bp" ]]
then 
    let brightness=brightness+passo
    if [[ $brightness -ge $max_brightness ]]
    then
       echo "Max : $max_brightness"
       echo $max_brightness > "$path_brightness/brightness"
    else
        echo " brightness : $brightness "
        echo $brightness > "$path_brightness/brightness"
    fi
elif [[ "$1" == "-bd" ]]
then
    let brightness=brightness-passo
    if [[ $min_brightness -lt $brightness ]]
    then
        echo "brightness : $brightness"
        echo $brightness > "$path_brightness/brightness"
    else
        echo "min : $min_brightness "
        echo $min_brightness > "$path_brightness/brightness"
        
    fi
fi
echo "Fine"
exit 0
