# Script kde connect

1. insert in group staff

    `sudo usermod -aG staff <your_user>`

2. in root insert in crontab -e

    `@reboot <path_dir>/kde_connect_script/crontab/change_permission.sh`

3. change permission in all file `.sh`

    `chmod 774 <file>`
4. Ceck your file 
    
5. insert command in kde connect 
    
    |name|command|
    |-----|-------|
    |spotify| bash <path_dir>/kde_connect_script/multi_app.sh spotify |

