#!/bin/bash
#
#Author: Domenico Caravaggio
BASE_DIR='/home/domenico/bin'
case $1 in
    "volume+"|"-vp")
    amixer sset 'Master' 20%+;;
    
    "volume-"|"-vd")
    amixer sset 'Master' 20%-;;
    
    "netflix"|"-n" )
    librewolf "https://www.netflix.com/browse"
    ;;

    "primevideo"|"-pr")
    librewolf "https://www.primevideo.com/"
    ;;

    "spotify"|"-s")
    spotify;;
    
    "youtube"|"-yt")
    librewolf "https://www.youtube.com/";;
    
    "-h"|"--help"|*)
    echo " multi_app.sh : uso"
    echo " multi_app.sh [ volume+ or -vp | volume- or -vd | netflix or -n | primevideo or -pr | spotify ] "
    echo " oppure ..."
    echo " CIAO ♥"
    ;;
esac
exit 0
